# Lab2 Calorie Daily

## 三个页面

<img src="https://images.gitee.com/uploads/images/2021/0513/191912_51d96395_7586726.png" alt="微信截图_20210513174618" style="zoom:50%;" /><img src="https://images.gitee.com/uploads/images/2021/0513/191951_2c25cdba_7586726.png" alt="微信截图_20210513174737" style="zoom:50%;" /><img src="https://images.gitee.com/uploads/images/2021/0513/192011_d86e37bd_7586726.png" alt="微信截图_20210513174806" style="zoom:50%;" />





## 数据库设计

数据库创建类

```kotlin
class DBHelper(ctx: Context, db: String, ver: Int) : SQLiteOpenHelper(ctx, db, null, ver) {
    private val version = ver
    private val create_tables = "create table notes (nid INTEGER PRIMARY KEY AUTOINCREMENT, time TEXT, content TEXT)"

    override fun onCreate(p0: SQLiteDatabase?) {
        updateDatabase(p0!!, 0, version)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        updateDatabase(p0!!, p1, p2)
    }

    private fun updateDatabase(db: SQLiteDatabase, ov: Int, nv: Int) {
        db.execSQL("drop table if exists notes");
        db.execSQL(create_tables)
    }
}
```

数据库访问类

```kotlin
class Note(i:Int, t:String, c:String){
    val nid = i//记录id，自增
    var time = t//记录时间
    var content = c//记录内容

    //以伴随对象方式定义一个Note类型数组列表notes
    companion object{
        val notes = ArrayList<Note>()
    }

    //输出Note实例的time和content值，该方法会被其他程序调用
    override fun toString(): String {
        return this.time+"： "+this.content
    }
}
```



## 添加数据操作
![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/192109_cfde22bf_7586726.png "微信截图_20210513175026.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/192116_a495a7c7_7586726.png "微信截图_20210513175054.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/192136_dd5f42cf_7586726.png "微信截图_20210513175118.png")


显示日期选择的界面

```xml
<DatePicker
    android:id="@+id/time"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content" />
```

点击按钮UPDATE时的事件

```kotlin
fun confirm(view: View) {
        //格式对齐
        var t = ""
        if (tv.month < 9) {
            t = "${tv.year}-0${tv.month + 1}-${tv.dayOfMonth}"
        }
        if (tv.dayOfMonth < 10) {
            t = "${tv.year}-${tv.month + 1}-0${tv.dayOfMonth}"
        }
        if (tv.month < 9 && tv.dayOfMonth < 10) {
            t = "${tv.year}-0${tv.month + 1}-0${tv.dayOfMonth}"
        }
        if (tv.month >= 9 && tv.dayOfMonth >= 10) {
            t = "${tv.year}-${tv.month + 1}-${tv.dayOfMonth}"
        }
//        val t = "${tv.year}-${tv.month + 1}-${tv.dayOfMonth}"
        val c = (cv.text).toString()
        val dba = DBAccessor.instance
        Log.i("insert[]", t)
        dba.insertNote(t, c)
        finish()
    }
```

增加数据的方法

```kotlin
//增加数据
fun insertNote(t:String, c:String){
    if (!isWritableDB())
        db = helper.writableDatabase
    val values = ContentValues()
    values.put("time", t)
    values.put("content", c)
    db.insert("notes", null, values)
}
```



## 删除数据操作

长按一条数据出现Remove提示，点击Remove后删除数据并自动刷新界面
![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/192237_5bb20631_7586726.png "微信截图_20210513184917.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/192246_f4c5271b_7586726.png "微信截图_20210513185052.png")


新建上下文菜单
![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/192301_f89ce374_7586726.png "微信截图_20210513184201.png")

```xml
<?xml version="1.0" encoding="utf-8"?>
<menu xmlns:android="http://schemas.android.com/apk/res/android">
    <item
        android:id="@+id/action_remove"
        android:title="Remove" />
</menu>
```

在页面2上显示数据并注册上下文菜单

![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/192315_6aabbe88_7586726.png "微信截图_20210513184400.png")

实现上下文菜单的删除功能

```kotlin
override fun onCreateContextMenu(
    menu: ContextMenu, v: View,
    menuInfo: ContextMenu.ContextMenuInfo
) {
    super.onCreateContextMenu(menu, v, menuInfo)
    menuInflater.inflate(R.menu.context_menu, menu)  //加载上下文菜单
}

override fun onContextItemSelected(item: MenuItem): Boolean {
    val id = item!!.itemId
    if (id == R.id.action_remove) {  //上下文菜单功能
        val info = item.menuInfo as AdapterView.AdapterContextMenuInfo
        val iid = info.id
        val i = Note.notes.get(iid.toInt()).nid
        dba.removeNote(i)  //删除数据
        reloadNotes()
        adapter.notifyDataSetChanged()  //更新界面显示
    }
    return super.onContextItemSelected(item)
}
```

删除数据的方法

```kotlin
//删除数据
fun removeNote(i:Int): Int {
    if (!isWritableDB())
        db = helper.writableDatabase
    return db.delete("notes","nid = ?",arrayOf(i.toString()))
}
```

刷新界面的方法

```kotlin
//加载数据
private fun reloadNotes() {
    if (Note.notes.size > 0)
        Note.notes.clear()
    dba.getNotes(Note.notes)
}

override fun onResume() {
    super.onResume()
    reloadNotes()
    adapter.notifyDataSetChanged()  //基于适配器更新界面显示
}
```



## 显示图表
![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/192341_c570b1c8_7586726.png "微信截图_20210513185421.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/192351_2723e581_7586726.png "微信截图_20210513185502.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/192358_74184e24_7586726.png "微信截图_20210513185528.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/192405_e082be55_7586726.png "微信截图_20210513185611.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/192412_8af2f98c_7586726.png "微信截图_20210513185645.png")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0513/192420_f213c3c2_7586726.png "微信截图_20210513185654.png")


根据选择的开始和结束日期查找数据

```kotlin
//查询beginDate和endDate之间的数据并放到result数组中
private fun loadData(): ArrayList<Record> {
    val dbHelper = DBHelper(this, "mydb", 1)
    // 取得一个只读的数据库对象
    val db: SQLiteDatabase = dbHelper.readableDatabase
    // 查询结果，最多4条
    val cursor: Cursor = db.rawQuery(
        "select * from notes where time >= ? and time <= ? limit 4",
        arrayOf(beginDate, endDate)
    )
    // 结果数组
    val result = arrayListOf<Record>()
    // 拿出数据放到result中
    val c = cursor.count
    if (c > 0) {
        while (cursor.moveToNext()) {
            //获取日期和数值(0：nid，1：time，2：content)
            val d = cursor.getString(1)
            val v = cursor.getString(2)
            val n = Record(d, v)
            // 加到result
            result.add(n)
        }
    }
    return result
}
```

按钮QUERY的点击事件

```kotlin
@SuppressLint("RestrictedApi")
@RequiresApi(Build.VERSION_CODES.O)
fun query(view: View) {
    if (this.begin == null || this.end == null) {
        // 日期其中一个为空，不渲染
    }
    if (this.beginDate > this.endDate) {
        //当开始日期大于结束日期，不渲染
    } else {
        val graph = findViewById<GraphView>(R.id.graph)
        graph.removeAllSeries()
        // 根据日期时间段查找数据记录，然后渲染
        val data = loadData()

        //数据类型转换并放进dataPointList列表
        val dataPointList = ArrayList<DataPoint>()
        for (i in data) {
            dataPointList.add(
                DataPoint(
                    SimpleDateFormat("yyyy-MM-dd").parse(i.mdate),
                    i.mvalue.toDouble()
                )
            )
        }
        // 把数据数组作为折线渲染数据
        val series: LineGraphSeries<DataPoint> = LineGraphSeries(dataPoint)
        // 渲染到图表
        graph.addSeries(series)

        graph.gridLabelRenderer.labelFormatter = DateAsXAxisLabelFormatter(this)
        graph.gridLabelRenderer.numHorizontalLabels = dataPoint.size

        //图表X轴设置
        graph.viewport.setMinX(dataPoint[0].x)
        graph.viewport.setMaxX(dataPoint[dataPoint.size - 1].x)
        // set manual x bounds to have nice steps
        graph.viewport.isXAxisBoundsManual = true

        // as we use dates as labels, the human rounding to nice readable numbers
        // is not necessary
        graph.gridLabelRenderer.setHumanRounding(true)
    }
}
```