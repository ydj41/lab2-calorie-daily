package com.example.caloriedaily

class Note(i:Int, t:String, c:String){
    val nid = i//记录id，自增
    var time = t//记录时间
    var content = c//记录内容

    //以伴随对象方式定义一个Note类型数组列表notes
    companion object{
        val notes = ArrayList<Note>()
    }

    //输出Note实例的time和content值，该方法会被其他程序调用
    override fun toString(): String {
        return this.time+"： "+this.content
    }
}
