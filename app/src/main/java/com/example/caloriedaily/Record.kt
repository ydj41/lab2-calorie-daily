package com.example.caloriedaily

class Record(d:String,v:String){
    val mdate = d
    val mvalue = v

    companion object{
        val record = ArrayList<Record>()
    }

    override fun toString(): String {
        return "Record(mdate='$mdate', mvalue='$mvalue')"
    }

}