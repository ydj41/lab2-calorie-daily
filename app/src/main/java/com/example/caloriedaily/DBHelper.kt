package com.example.caloriedaily

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBHelper(ctx: Context, db: String, ver: Int) : SQLiteOpenHelper(ctx, db, null, ver) {
    private val version = ver
    private val create_tables = "create table notes (nid INTEGER PRIMARY KEY AUTOINCREMENT, time TEXT, content TEXT)"

    override fun onCreate(p0: SQLiteDatabase?) {
        updateDatabase(p0!!, 0, version)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        updateDatabase(p0!!, p1, p2)
    }

    private fun updateDatabase(db: SQLiteDatabase, ov: Int, nv: Int) {
        db.execSQL("drop table if exists notes");
        db.execSQL(create_tables)
    }
}
