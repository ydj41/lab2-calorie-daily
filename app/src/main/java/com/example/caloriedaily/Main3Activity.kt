package com.example.caloriedaily

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.DatePicker
import android.widget.EditText

class Main3Activity : AppCompatActivity() {
    lateinit var tv: DatePicker
    lateinit var cv: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main3)
        tv = findViewById<DatePicker>(R.id.time)
        cv = findViewById<EditText>(R.id.content)
    }

    fun confirm(view: View) {
        //格式对齐
        var t = ""
        if (tv.month < 9) {
            t = "${tv.year}-0${tv.month + 1}-${tv.dayOfMonth}"
        }
        if (tv.dayOfMonth < 10) {
            t = "${tv.year}-${tv.month + 1}-0${tv.dayOfMonth}"
        }
        if (tv.month < 9 && tv.dayOfMonth < 10) {
            t = "${tv.year}-0${tv.month + 1}-0${tv.dayOfMonth}"
        }
        if (tv.month >= 9 && tv.dayOfMonth >= 10) {
            t = "${tv.year}-${tv.month + 1}-${tv.dayOfMonth}"
        }
//        val t = "${tv.year}-${tv.month + 1}-${tv.dayOfMonth}"
        val c = (cv.text).toString()
        val dba = DBAccessor.instance
        Log.i("insert[]", t)
        dba.insertNote(t, c)
        finish()
    }
}
